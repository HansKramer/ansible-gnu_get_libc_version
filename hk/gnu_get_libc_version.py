#!/usr/bin/python
# -*- coding: utf-8 -*-

# (c) 2019, Hans Kramer <jlam.kramer@gmail.com>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from __future__ import absolute_import, division, print_function
__metaclass__ = type


ANSIBLE_METADATA = {
    'metadata_version' : '1.1',
    'status'           : ['preview'],
    'supported_by'     : 'community'
}


DOCUMENTATION = '''
---
module: gnu_get_libc_version
version_added: 2019
short_description: Determine gnu_libc_version
description:
   - return the glibc version 
notes:
   - For Windows targets, wipe your system install Linux
options:
  compare:
     description:
       - compare with version
author:
    - Hans Kramer
'''

EXAMPLES = '''
# Example from an Ansible Playbook
- gnu_get_libc_version:
      compare: '2.27'
'''

RETURN = '''
version:
    description: The version of glibc
    returned: version
    type: string
    sample: 2.27

result:
    description: result of comparison: -1 smaller, 0 equal, 1 greater  
    returned: result
    type: int
    sample: -1
'''


from ansible.module_utils.basic import AnsibleModule

import ctypes

from ctypes import *


def get_version():
    try:
        libc = ctypes.cdll.LoadLibrary("libc.so.6")

        if not hasattr(libc, "gnu_get_libc_version"):
            raise OSError("attribute error : gnu_get_libc_version")

        libc.gnu_get_libc_version.restype = ctypes.c_char_p

        return libc.gnu_get_libc_version()
    except OSError:
        return None


def version_compare(test_version, version):
    s_test_version = test_version.split('.')
    s_version      = version.split('.')
    if len(s_test_version) != len(s_version):
        return True, "cannot compare : %s with %s" % (test_version, version)
    for n in s_test_version:
        if not n.isdigit():
            return True, "version must be a number : %s" % test_version       

    for n in range(len(s_test_version)):
        if s_test_version[n] > s_version[n]:
            return False, -1
        elif s_test_version[n] < s_version[n]:
            return False, 1

    return False, 0


def main():
    module = AnsibleModule(
        argument_spec=dict(
             compare=dict(type='str'),
        ),
        supports_check_mode=True
    )


    version = get_version()
 
    result = dict(
        version = version,
        failed  = version is None
    )

    if version is not None and module.params['compare']:
         result['failed'], retval = version_compare(module.params['compare'], version)
         if not result['failed']:
             result['result'] = retval
         else:
             result['error_message'] = retval

    module.exit_json(**result)


if __name__ == '__main__':
    main()
